module bitbucket.org/uwaploe/nmeadist

require (
	github.com/BurntSushi/toml v0.3.1
	github.com/armon/go-metrics v0.0.0-20180917152333-f0300d1749da // indirect
	github.com/go-sql-driver/mysql v1.4.1 // indirect
	github.com/gogo/protobuf v1.2.1 // indirect
	github.com/golang/protobuf v1.3.1 // indirect
	github.com/hashicorp/go-immutable-radix v1.0.0 // indirect
	github.com/hashicorp/go-msgpack v0.5.3 // indirect
	github.com/hashicorp/raft v1.0.0 // indirect
	github.com/lib/pq v1.0.0 // indirect
	github.com/nats-io/gnatsd v1.4.1 // indirect
	github.com/nats-io/go-nats v1.7.2 // indirect
	github.com/nats-io/go-nats-streaming v0.4.2
	github.com/nats-io/nats-streaming-server v0.12.2 // indirect
	github.com/nats-io/nkeys v0.0.2 // indirect
	github.com/nats-io/nuid v1.0.0 // indirect
	github.com/pascaldekloe/goe v0.1.0 // indirect
	github.com/prometheus/procfs v0.0.0-20190328153300-af7bedc223fb // indirect
	github.com/tarm/serial v0.0.0-20180830185346-98f6abe2eb07
	go.etcd.io/bbolt v1.3.2 // indirect
	golang.org/x/sys v0.0.0-20190402142545-baf5eb976a8c // indirect
	google.golang.org/appengine v1.5.0 // indirect
)

go 1.13
