package main

import (
	"errors"
	"io"
	"net"
	"time"
)

type NmeaSink struct {
	input chan []byte
	err   error
	done  chan struct{}
	wtr   io.Writer
}

var ErrChan = errors.New("Channel closed")

func (sink *NmeaSink) listen() {
	defer close(sink.done)
	for {
		select {
		case b := <-sink.input:
			if conn, ok := sink.wtr.(net.Conn); ok {
				conn.SetWriteDeadline(time.Now().Add(time.Millisecond * 200))
			}
			sink.wtr.Write(b)
			_, sink.err = sink.wtr.Write([]byte("\r\n"))
			if sink.err != nil {
				close(sink.input)
				sink.input = nil
				return
			}
		}
	}
}

func (sink *NmeaSink) Err() error {
	return sink.err
}

func (sink *NmeaSink) Done() chan struct{} {
	return sink.done
}

func (sink *NmeaSink) Send(b []byte) error {
	select {
	case <-sink.Done():
		return sink.Err()
	case sink.input <- b:
	default:
		return ErrChan
	}

	return nil
}

func NewSink(w io.Writer, backlog int) *NmeaSink {
	sink := &NmeaSink{
		input: make(chan []byte, backlog),
		done:  make(chan struct{}),
		wtr:   w,
	}

	go sink.listen()

	return sink
}
