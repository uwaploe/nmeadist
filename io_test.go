package main

import (
	"bufio"
	"log"
	"net"
	"testing"
)

func startReceiver(t *testing.T) (net.Listener, chan struct{}) {
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		t.Fatal(err)
	}

	done := make(chan struct{}, 1)
	go func() {
		defer close(done)
		conn, _ := listener.Accept()
		defer conn.Close()

		rdr := bufio.NewReader(conn)
		b, _ := rdr.ReadBytes('\n')
		log.Printf("got: %q", b)
	}()

	return listener, done
}

func TestSink(t *testing.T) {
	l, done := startReceiver(t)
	defer l.Close()

	addr := l.Addr().String()
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		t.Fatal(err)
	}
	s := NewSink(conn, 4)

	s.Send([]byte("hello world"))
	select {
	case <-s.Done():
		t.Fatalf("Unexpected error: %v", s.Err())
	default:
	}
	<-done

	for i := 0; i < 10; i++ {
		s.Send([]byte("goodbye"))
	}
	err = s.Send([]byte("goodbye again"))
	if err != ErrChan {
		t.Error("Error not caught")
	}

}
