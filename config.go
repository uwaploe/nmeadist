package main

import (
	"fmt"
	"io/ioutil"

	"github.com/BurntSushi/toml"
)

//
// Example configuration format
//
// [nats]
// url = "nats://192.9.0.1:4222"
// cluster = "must-cluster"
// [sink]
// name = "192.9.0.13:4001"
// [[sink.input]]
// subject = "nmea.survey"
// [[sink.input]]
// subject = "nmea.focus"

type natsConfig struct {
	Url     string `toml:"url"`
	Cluster string `toml:"cluster"`
}

type sysConfig struct {
	Nats natsConfig `toml:"nats"`
	Sink sinkDesc   `toml:"sink"`
}

type sinkDesc struct {
	Name  string      `toml:"name"`
	Baud  int         `toml:"baud"`
	Input []inputDesc `toml:"input"`
}

type inputDesc struct {
	Subject string `toml:"subject"`
	Tag     string `toml:"tag,omitempty"`
}

func loadConfig(filename string) (sysConfig, error) {
	var cfg sysConfig
	b, err := ioutil.ReadFile(filename)
	if err != nil {
		return cfg, fmt.Errorf("read %q: %w", filename, err)
	}

	err = toml.Unmarshal(b, &cfg)
	return cfg, err
}
