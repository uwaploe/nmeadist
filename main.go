// Nmeadist subscribes to NMEA sentences published to a NATS Streaming
// Server and distributes them to one or more serial ports.
package main

import (
	"bytes"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"path/filepath"
	"runtime"
	"strings"
	"syscall"

	stan "github.com/nats-io/go-nats-streaming"
)

var Version = "dev"
var BuildDate = "unknown"

const Usage = `Usage: nmeadist [options] configfile

Subscribes to NMEA sentences published to a NATS Streaming
Server and distributes them to a serial or TCP port.
`

var (
	showVers = flag.Bool("version", false,
		"Show program version information and exit")
)

func openPort(d sinkDesc) *NmeaSink {
	var (
		p   Port
		err error
	)

	if strings.Contains(d.Name, ":") {
		p, err = NetworkPort(d.Name)
	} else {
		if d.Baud == 0 {
			d.Baud = 4800
		}
		p, err = SerialPort(d.Name, d.Baud)
	}

	if err != nil {
		log.Fatalf("Cannot open port %q: %v", d.Name, err)
	}

	return NewSink(p, len(d.Input))
}

func parseCmdLine() []string {
	flag.Usage = func() {
		fmt.Fprintf(os.Stderr, Usage)
		flag.PrintDefaults()
	}

	flag.Parse()

	if *showVers {
		fmt.Fprintf(os.Stderr, "%s %s\n", os.Args[0], Version)
		fmt.Fprintf(os.Stderr, "  Built with: %s\n", runtime.Version())
		os.Exit(0)
	}

	return flag.Args()
}

func main() {
	args := parseCmdLine()
	if len(args) == 0 {
		flag.Usage()
		os.Exit(1)
	}

	cfg, err := loadConfig(args[0])
	if err != nil {
		log.Fatal(err)
	}

	cname := fmt.Sprintf("nmea-dist-%s",
		strings.Replace(filepath.Base(args[0]), ".", "_", -1))

	sc, err := stan.Connect(cfg.Nats.Cluster, cname, stan.NatsURL(cfg.Nats.Url))
	if err != nil {
		log.Fatalf("Cannot connect: %v", err)
	}
	defer sc.Close()

	// Initialize signal handler
	sigs := make(chan os.Signal, 1)
	signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGHUP)
	defer signal.Stop(sigs)

	sink := openPort(cfg.Sink)

	// Subscribe to all message subjects
	subs := make([]stan.Subscription, 0)
	for _, inp := range cfg.Sink.Input {
		cb := func(m *stan.Msg) {
			b := bytes.SplitN(m.Data, []byte(":"), 2)
			if inp.Tag != "" {
				if string(b[0]) == inp.Tag {
					sink.Send(b[1])
				}
			} else {
				sink.Send(b[0])
			}
		}
		sub, err := sc.Subscribe(inp.Subject, cb)
		if err != nil {
			log.Fatalf("Cannot subscribe to %q: %v", inp.Subject, err)
		}
		subs = append(subs, sub)
	}

	log.Printf("NMEA distribution service starting %s", Version)

	select {
	case <-sink.Done():
		log.Printf("Write error: %v", sink.Err())
	case s := <-sigs:
		log.Printf("Got signal: %v", s)
	}

	for _, sub := range subs {
		sub.Unsubscribe()
	}
}
